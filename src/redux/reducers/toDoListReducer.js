import { arrTheme } from "../../themes/ThemeManager";
import { ToDoListDarkTheme } from "../../themes/ToDoListDarkTheme";
import {
  ADD_TASK,
  CHANGE_THEME,
  COMPlETED_TASK,
  DELETE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../types/toDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: 1, taskName: "task1", done: false },
    { id: 2, taskName: "task2", done: false },
    { id: 3, taskName: "task3", done: true },
    { id: 4, taskName: "task4", done: true },
  ],
  taskEdit: { id: "-1", taskName: "", done: false },
};

export const toDoListReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TASK: {
      if (action.payload.taskName === "") {
        alert("Plese enter task name");
        return { ...state };
      }
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex((task) => {
        return task.taskName === action.payload.taskName;
      });
      if (index !== -1) {
        alert("Task name already exist");
        return { ...state };
      } else {
        taskListUpdate.push(action.payload);
      }
      return { ...state, taskList: taskListUpdate };
    }
    case CHANGE_THEME: {
      let theme = arrTheme.find((theme) => theme.id == action.payload);
      return { ...state, themeToDoList: theme.theme };
    }
    case DELETE_TASK: {
      let cloneTask = [...state.taskList];
      let index = cloneTask.findIndex((task) => task.id === action.payload);
      if (index !== -1) {
        cloneTask.splice(index, 1);
      }
      return { ...state, taskList: cloneTask };
    }
    case COMPlETED_TASK: {
      let cloneTask = [...state.taskList];
      let index = cloneTask.findIndex((task) => task.id === action.payload);
      cloneTask[index].done = true;
      return { ...state, taskList: cloneTask };
    }
    case EDIT_TASK: {
      return { ...state, taskEdit: action.payload };
    }
    case UPDATE_TASK: {
      let cloneTask = [...state.taskList];
      let index = cloneTask.findIndex((task) => task.id === state.taskEdit.id);
      if (index !== -1) {
        cloneTask[index].taskName = action.payload;
      }
      return {
        ...state,
        taskList: cloneTask,
        taskEdit: { id: "-1", taskName: "", done: false },
      };
    }
    default:
      return { ...state };
  }
};
