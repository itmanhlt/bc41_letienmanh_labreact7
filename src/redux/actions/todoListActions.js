import {
  ADD_TASK,
  CHANGE_THEME,
  COMPlETED_TASK,
  DELETE_TASK,
  EDIT_TASK,
  UPDATE_TASK,
} from "../types/toDoListTypes";

export const addTaskAction = (payload) => ({
  type: ADD_TASK,
  payload,
});

export const changeThemeAction = (payload) => ({
  type: CHANGE_THEME,
  payload,
});

export const deleteTaskAction = (payload) => ({
  type: DELETE_TASK,
  payload,
});

export const completedTaskAction = (payload) => ({
  type: COMPlETED_TASK,
  payload,
});

export const editTaskAction = (payload) => ({
  type: EDIT_TASK,
  payload,
});

export const updateTaskAction = (payload) => ({
  type: UPDATE_TASK,
  payload,
});
