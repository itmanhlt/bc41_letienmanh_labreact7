import React, { Component } from "react";
import { Container } from "../Container/Container";
import { ThemeProvider } from "styled-components";
// import { ToDoListDarkTheme } from "../themes/ToDoListDarkTheme";
// import { ToDoListLightTheme } from "../themes/ToDoListLightTheme";
// import { ToDoListPrimaryTheme } from "../themes/ToDoListPrimaryTheme";
import { Dropdown } from "../Components/Dropdown";
import { Heading3 } from "../Components/Heading";
import { TextField } from "../Components/TextField";
import { Button } from "../Components/Button";
import { Table, Tr, Th, Thead } from "../Components/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
  completedTaskAction,
  deleteTaskAction,
  editTaskAction,
  updateTaskAction,
} from "../redux/actions/todoListActions";
import { arrTheme } from "../themes/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.setState({ disabled: false }, () => {
                    this.props.dispatch(editTaskAction(task));
                  });
                }}
              >
                <i className="fa fa-edit"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(completedTaskAction(task.id));
                }}
              >
                <i className="fa fa-check"></i>
              </Button>
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              >
                <i className="fa fa-trash"></i>
              </Button>
            </Th>
          </Tr>
        );
      });
  };

  renderTheme = () => {
    return arrTheme.map((theme, index) => {
      return (
        <option key={index} value={theme.id}>
          {theme.name}
        </option>
      );
    });
  };

  render() {
    return (
      <div>
        <ThemeProvider theme={this.props.themeToDoList}>
          <Container className="w-50">
            <Dropdown
              onChange={(e) => {
                let { value } = e.target;
                this.props.dispatch(changeThemeAction(value));
              }}
            >
              {this.renderTheme()}
            </Dropdown>
            <Heading3>To do list</Heading3>
            <TextField
              onChange={(e) => {
                this.setState({ taskName: e.target.value });
              }}
              value={this.state.taskName}
              name="taskName"
              label="Task name"
              className="w-50"
            />
            <Button
              onClick={() => {
                let { taskName } = this.state;
                let newTask = {
                  id: Date.now(),
                  taskName: taskName,
                  done: false,
                };
                this.props.dispatch(addTaskAction(newTask));
              }}
              className="ml-2"
            >
              <i className="fa fa-plus mr-2"></i>Add task
            </Button>
            {this.state.disabled ? (
              <Button
                disabled
                onClick={() => {
                  this.props.dispatch(updateTaskAction(this.state.taskName));
                }}
                className="ml-2"
              >
                <i className="fa fa-upload mr-2"></i>Update task
              </Button>
            ) : (
              <Button
                onClick={() => {
                  let { taskName } = this.state;
                  this.setState({ disabled: true, taskName: "" }, () => {
                    this.props.dispatch(updateTaskAction(taskName));
                  });
                }}
                className="ml-2"
              >
                <i className="fa fa-upload mr-2"></i>Update task
              </Button>
            )}
            <Heading3>Task to do</Heading3>
            <Table>
              <Thead>{this.renderTaskToDo()}</Thead>
            </Table>
            <Heading3>Task completed</Heading3>
            <Table>
              <Thead>{this.renderTaskCompleted()}</Thead>
            </Table>
          </Container>
        </ThemeProvider>
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.taskEdit.id !== this.props.taskEdit.id) {
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
    }
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.toDoListReducer.themeToDoList,
    taskList: state.toDoListReducer.taskList,
    taskEdit: state.toDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
